var express        = require('express');
var router         = express.Router();
var ctrl           = require('../controllers/login');
var fs             = require('fs');
var verifyToken    = require('./middleware');
var jwt            = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config         = require('./config');

//router.post('/', verifyToken, doSome)

router.post('/', doMaster);
router.post('/create-user', doCreateUser);
router.post('/grant-user', doGrantUser);

function doMaster (req, res) {
  var d = req.body;
  console.log(d,'*****************************');
  ctrl.doMaster(d)
  .then(function(result){
      res.json(result);
  })
}

function doCreateUser(req, res) {
    var d = req.body;
    ctrl.doCreateUser(d)
    .then(function(result){
        res.json(result);
    })
}

function doGrantUser(req, res) {
    var d = req.body;
    ctrl.doGrantUser(d)
    .then(function(result){
        res.json(result);
    })
}

module.exports = router;
