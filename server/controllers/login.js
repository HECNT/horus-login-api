var model = require('../models/login');

module.exports.doMaster = function(d) {
    return new Promise(function(resolve, reject){
        if ( d.user === undefined || d.holi === undefined || d.database === undefined ) {
            resolve({ err: true, description: 'Json invalido' });
        } else {
            model.doMaster(d)
            .then(function(res){
                resolve(res);
            })
        }
    })
}

module.exports.doCreateUser = function(d) {
    return new Promise(function(resolve, reject){
        if ( d.user === undefined || d.holi === undefined || d.database === undefined || d.rol === undefined || d.user_create === undefined) {
            resolve({ err: true, description: 'Json invalido' });
        } else {
            model.doCreateUser(d)
            .then(function(res){

                model.doCreateUserWorld(d)
                .then(function(res){
                    if ( d.rol === 'admin' ) {
                        d.rol = 'ALL';
                    }
    
                    if ( d.rol === 'reader' ) {
                        d.rol = 'SELECT';
                    }
    
                    if ( d.rol === 'writer' ) {
                        d.rol = 'SELECT, INSERT, UPDATE';
                    }
    
                    model.addRolUser(d)
                    .then(function(res){
                        model.addRolUserWorld(d)
                        .then(function(res){
                            model.flush(d)
                            .then(function(res){
                                resolve(res)
                            })
                        })
                    })
                })
            })
        }
    })
}

module.exports.doGrantUser = function(d) {
    return new Promise(function(resolve, reject){
        if ( d.user === undefined || d.holi === undefined || d.database === undefined ) {
            resolve({ err: true, description: 'Json invalido' });
        } else {
            model.addRolUser(d)
            .then(function(res){
                model.addRolUserWorld(d)
                .then(function(res){
                    resolve(res);
                })
            })
        }
    })
}