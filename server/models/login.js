var mysql = require('mysql');
var fs = require('fs');

module.exports.doMaster = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database
        console.log(conn,' **********************');
        
        var connection = mysql.createConnection(conn);

        connection.connect();

        connection.query('SELECT 1+1 AS res', function(err, result){
            if (err) {
                resolve({err: true, description: err});
            } else {
                resolve({err: false, data: result});
            }
        })
    })
}

module.exports.doCreateUser = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database

        var connection = mysql.createConnection(conn);

        connection.connect();

        var query = `CREATE USER '${d.user_create}'@'localhost' IDENTIFIED BY 'HORUS1234';`
        console.log(query, 'CREAR USUARIO 1');
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
            } else {
                resolve({err: false, data: result});
            }
        })
    })
}

module.exports.doCreateUserWorld = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database

        var connection = mysql.createConnection(conn);

        connection.connect();

        var query = `CREATE USER '${d.user_create}'@'%' IDENTIFIED BY 'HORUS1234';`
        console.log(query, 'CREAR USUARIO 2');
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
            } else {
                resolve({err: false, data: result});
            }
        })
    })
}

module.exports.addRolUser = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database

        var aBase = d.database_add === undefined ? d.database : d.database_add

        var connection = mysql.createConnection(conn);

        connection.connect();

        var query = `GRANT ${d.rol} ON ${aBase}.* TO '${d.user_create}'@'localhost';`
        console.log(query,'Query');
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
            } else {
                resolve({err: false, data: result});
            }
        })
    })
}

module.exports.addRolUserWorld = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database

        var aBase = d.database_add === undefined ? d.database : d.database_add

        var connection = mysql.createConnection(conn);

        connection.connect();

        var query = `GRANT ${d.rol} ON ${aBase}.* TO '${d.user_create}'@'%' ;`
        console.log(query,'QUERY2');
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
            } else {
                resolve({err: false, data: result});
            }
        })
    })
}

module.exports.flush = function(d) {
    return new Promise(function(resolve, reject){
        var conn = fs.readFileSync(__dirname + '/conn') ;
        conn = JSON.parse(conn);
        
        conn.user = d.user
        conn.password = d.holi
        conn.database = d.database

        var connection = mysql.createConnection(conn);

        connection.connect();

        var query = `FLUSH PRIVILEGES;`
        console.log(query, 'QUERY FINAL');
        connection.query(query, function(err, result){
            if (err) {
                resolve({err: true, description: err});
            } else {
                resolve({err: false, data: result});
            }
        })
    })
}

